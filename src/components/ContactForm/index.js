// Libraries
import React from 'react';
import injectSheet from 'react-jss';

// Styles
import styles from './style';

export class ContactForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      message: '',
      sent: props.sent || false,
      error: props.error || ''
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();

    // Do some client-side validation

    this.setState({
      sent: true
    });
  }

  handleChange(e) {
    this.setState({
      [e.target.id]: e.target.value
    });
  }

  render() {
    const { classes = {} } = this.props;

    return (
      <form className={classes.form} onSubmit={this.handleSubmit}>
        <h2>Contact Us</h2>
        {this.state.error && (
          <div className={classes.error}>{this.state.error}</div>
        )}
        {this.state.sent ? (
          <div className={classes.success}>
            Your message has been sent successfully!
          </div>
        ) : (
          <React.Fragment>
            <label htmlFor="name">Name</label>
            <input
              type="text"
              name="name"
              id="name"
              value={this.state.name}
              onChange={this.handleChange}
            />

            <label htmlFor="email">Email</label>
            <input
              type="email"
              name="email"
              id="email"
              value={this.state.email}
              onChange={this.handleChange}
            />

            <label htmlFor="message">Message</label>
            <textarea
              name="message"
              id="message"
              onChange={this.handleChange}
              value={this.state.message}
            />

            <input type="submit" value="Send Message" />
          </React.Fragment>
        )}
      </form>
    );
  }
}

export default injectSheet(styles)(ContactForm);
