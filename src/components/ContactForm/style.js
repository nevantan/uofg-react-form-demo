export default ({ color, font, spacing }) => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
    fontFamily: font.family.sans,
    padding: spacing.s,

    '& label': {
      color: color.lighttext,
      fontSize: '1.4rem'
    },

    '& input, & textarea': {
      border: `1px solid ${color.lightborder}`,
      borderRadius: '2px',
      font: 'inherit',
      marginBottom: spacing.s,
      padding: spacing.xs
    }
  },
  error: {
    backgroundColor: color.errorback,
    borderRadius: '2px',
    color: color.errortext,
    marginBottom: spacing.m,
    padding: spacing.s
  },
  success: {
    backgroundColor: color.successback,
    borderRadius: '2px',
    color: color.successtext,
    marginBottom: spacing.m,
    padding: spacing.s
  }
});
