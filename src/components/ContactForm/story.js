import React from 'react';
import { storiesOf } from '@storybook/react';
import ContactForm from './';

storiesOf('Contact Form', module)
  .add('unsent', () => <ContactForm />)
  .add('sent', () => <ContactForm sent={true} />)
  .add('error', () => <ContactForm error="Test error message" />);
