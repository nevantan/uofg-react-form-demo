import React from 'react';
import { shallow } from 'enzyme';

import { ContactForm } from './';

describe('the ContactForm component', () => {
  const classes = {
    form: 'form',
    error: 'error',
    success: 'success'
  };

  it('should render correctly by default', () => {
    const wrapper = shallow(<ContactForm />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render correctly in success state', () => {
    const wrapper = shallow(<ContactForm sent={true} classes={classes} />);
    expect(wrapper.find('.success').length).toBe(1);
  });

  it('should render correctly in error state', () => {
    const wrapper = shallow(
      <ContactForm error="Test error message." classes={classes} />
    );
    expect(wrapper.find('.error').length).toBe(1);
  });

  it('should handle input correctly', () => {
    const wrapper = shallow(<ContactForm />);
    wrapper.find('#name').simulate('change', {
      target: {
        id: 'name',
        value: 'John Doe'
      }
    });
  });

  it('should handle submission correctly', () => {
    const preventDefault = jest.fn();

    const wrapper = shallow(<ContactForm classes={classes} />);
    wrapper.find('.form').simulate('submit', {
      preventDefault
    });

    expect(preventDefault).toHaveBeenCalledTimes(1);
    expect(wrapper.state('sent')).toBe(true);
  });
});
