// Libraries
import React from 'react';
import ReactDOM from 'react-dom';

// Styles
import { ThemeProvider } from 'react-jss';
import theme from './theme';

// Components
import ContactForm from './components/ContactForm';

const jsx = (
  <ThemeProvider theme={theme}>
    <ContactForm />
  </ThemeProvider>
);
const container = document.getElementById('react-form');

if (container !== null) {
  ReactDOM.render(jsx, container);
}
