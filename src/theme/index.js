const color = {
  errorback: '#800',
  errortext: '#fff',
  lightborder: '#ccc',
  lighttext: '#555',
  successback: '#080',
  successtext: '#fff'
};

const font = {
  family: {
    sans: '"Helvetica", sans-serif'
  }
};

const spacing = {
  xs: '0.5rem',
  s: '1rem',
  m: '2rem'
};

export default {
  color,
  font,
  spacing
};
