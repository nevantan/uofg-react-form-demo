module.exports = {
  setupFiles: ['<rootDir>/jest.setup.js'],
  testMatch: ['**/*.test.js'],
  collectCoverageFrom: ['src/components/**/index.js'],
  snapshotSerializers: ['enzyme-to-json/serializer']
};
