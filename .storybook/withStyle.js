import React from 'react';
import { ThemeProvider } from 'react-jss';
import theme from '../src/theme';

export default (story) => (
  <ThemeProvider theme={theme}>{story()}</ThemeProvider>
);
