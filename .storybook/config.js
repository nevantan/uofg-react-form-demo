import { configure, addDecorator } from '@storybook/react';
import withStyle from './withStyle';

const loadStories = () => {
  addDecorator(withStyle);
  require('../src/components/ContactForm/story.js');
};

configure(loadStories, module);
